package internal

import (
	"fmt"
	"os"
)

type uriPollID struct {
	ID string `uri:"pollID" binding:"required"`
}

// isSafe returns true if the supplied uri has bound an id of the valid type for polls
func (uri uriPollID) isSafe() bool {
	return IsMB64E(uri.ID)
}

// Poll represents a poll as saved in the json db
type Poll struct {
	ID            string       `json:"id"`
	Title         string       `json:"title"`
	Description   string       `json:"description"`
	Type          string       `json:"type"`
	MultiNum      int          `json:"multiNum"`
	AntiDupe      string       `json:"antiDupe"`
	Active        bool         `json:"active"`
	ResultsPublic bool         `json:"resultsPublic"`
	TotalVotes    int          `json:"totalVotes"`
	Options       []PollOption `json:"options"`
}

// PollOption is a votable option in a Poll
type PollOption struct {
	Name  string `json:"name"`
	Votes int    `json:"votes"`
}

// LoadPoll loaded the Poll targeted by the id
func LoadPoll(id string) (*Poll, bool) {
	var poll Poll
	err := DC.Load(getPollPath(id), &poll)
	if err != nil {
		return nil, false
	}
	return &poll, true
}

// SavePoll saves the given poll to the path with the target id, returns true if successful
func SavePoll(id string, poll *Poll) bool {
	return notErr(DC.Save(getPollPath(id), &poll))
}

// DeletePoll removes a poll from the db, returns true if successful
func DeletePoll(id string) bool {
	err := DC.Delete(getPollPath(id))
	ok := deleteAntiDupe(id)
	return notErr(err) && ok
}

// PollExists returns true if the poll with the target id exists, false otherwise
func PollExists(id string) bool {
	if _, err := os.Stat(getPollPath(id)); os.IsNotExist(err) {
		return false
	}
	return true
}

func getPollPath(id string) string {
	return "data/polls/" + id + ".json"
}

// GetTypeStr returns a string representation of the type of this poll
// this is a helper function for the template front-end
func (poll Poll) GetTypeStr() string {
	if poll.Type == "multi" {
		multiadditive := ""
		if poll.MultiNum > 1 {
			multiadditive = "1-"
		}
		plural := ""
		if poll.MultiNum > 1 {
			plural = "s"
		}
		return fmt.Sprintf("Choose %s%d answer%s:", multiadditive, poll.MultiNum, plural)
	} else if poll.Type == "radio" {
		return "Choose one answer:"
	} else {
		return "undefined type:"
	}
}

// IsMulti returns true if this Poll is of type multi
// this is a helper function for the template front-end
func (poll Poll) IsMulti() bool {
	switch poll.Type {
	case "multi":
		return true
	case "radio":
		return false
	default:
		return true
	}
}

// GetPBarColorClass returns a green bootstrap class if this option is the first, else gray
// this is a helper function for the template front-end
func (option PollOption) GetPBarColorClass(i int) string {
	if i == 0 {
		return " bg-success"
	}
	return " bg-secondary"
}
