package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// RequireSessionAuthn will redirect and cancel to login page if credentials are missing
// place before the endpoint of an authn group or appropriate handlers that require authentication
func RequireSessionAuthn() gin.HandlerFunc {
	return func(c *gin.Context) {
		// test for authentication and serve error if not provided
		if sessionGetUserID(c) == "" {
			SRedirect(c, http.StatusFound, GCONF.WebRoot+"/login")
			c.Abort()
		}
	}
}
