package internal

import (
	"crypto/rand"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/RememberOfLife/jdba"
)

var (
	// DC is the internal JDBA api
	DC *jdba.DC
	// GCONF is the static instance of the global config
	GCONF MainConfig
)

// MainConfig is a representation of the global configuration for this servers internals
type MainConfig struct {
	WebRoot          string `json:"webRoot"`
	LocalPort        string `json:"localPort"`
	StatusAuthzToken string `json:"statusAuthzToken"`
	DebugMode        bool   `json:"debugMode"`
}

func init() {
	DC = jdba.NewJDBA(300, false)
	err := DC.LoadSingle("config.json", &GCONF)
	if err != nil {
		DC.LoadSingle("default-config.json", &GCONF)
	}
	DC.Minify = !GCONF.DebugMode
}

// GetWebRoot returns the target webRoot for link building as given in the config
func GetWebRoot() string {
	return GCONF.WebRoot
}

// DBGGetDBAccessMap serves the acces map of the jdba dc
func DBGGetDBAccessMap(c *gin.Context) {
	c.JSON(http.StatusOK, DC.GetAccessMap())
}

type sessionKeyPair struct {
	AuthNKey string `json:"AuthNKey"`
	CryptKey string `json:"CryptKey"`
}

//SessionGetKeys returns the 4 session keys, ako and eko being nil if they dont exist, returns 0 in debug mode
func SessionGetKeys() (ak, ek, ako, eko []byte) {
	if GCONF.DebugMode {
		return make([]byte, 64), make([]byte, 32), nil, nil
	}
	// get old keys
	ako = nil
	eko = nil
	var oldSessionKeyPair sessionKeyPair
	err := DC.LoadSingle("sessionkeys.json", &oldSessionKeyPair)
	if notErr(err) {
		ako, _ = MB64E.DecodeString(oldSessionKeyPair.AuthNKey)
		eko, _ = MB64E.DecodeString(oldSessionKeyPair.CryptKey)
	}
	// gen new keys
	ak = make([]byte, 64) // auth key
	rand.Read(ak)
	ek = make([]byte, 32) // enc key
	rand.Read(ek)
	// save new keys
	newSessionKeyPair := sessionKeyPair{MB64E.EncodeToString(ak), MB64E.EncodeToString(ek)}
	DC.SaveSingle("sessionkeys.json", &newSessionKeyPair)
	return
}
