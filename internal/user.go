package internal

import (
	"crypto/rand"
	"crypto/sha512"
)

// User represents a valid user of this service as saved in the users db
type User struct {
	ID           string `json:"userID"`
	Salt         string `json:"salt"`
	PasswordHash string `json:"passwordHash"`
}

// LoadUser creates a User from the target id if they exist
func LoadUser(id string) (*User, bool) {
	var user User
	err := DC.Load(getUserPath(id), &user)
	if err != nil {
		return nil, false
	}
	return &user, true
}

// Save saves a user to the its id, returns true if successful
func (user *User) Save() bool {
	if user == nil {
		return false
	}
	return notErr(DC.Save(getUserPath(user.ID), &user))
}

func getUserPasswordHash(salt string, pw string) (string, bool) {
	hasher := sha512.New()
	saltBytes, err := MB64E.DecodeString(salt)
	_, err = hasher.Write(saltBytes)
	_, err = hasher.Write([]byte(pw))
	if isErr(err) {
		return "", false
	}
	return MB64E.EncodeToString(hasher.Sum(nil)), true
}

// CreateUser attempts to create a new user with the supplied id and password, returns true if successful
// existing users with the same id are overwritten as this is currently the only way to set pws
func CreateUser(id string, pw string) bool {
	user := new(User)
	user.ID = id
	if !IsFileSafe(id) {
		return false
	}
	// create new salt
	salt := make([]byte, 64)
	rand.Read(salt)
	user.Salt = MB64E.EncodeToString(salt)
	// set passwordhash
	hash, ok := getUserPasswordHash(user.Salt, pw)
	if !ok {
		return false
	}
	user.PasswordHash = hash
	return user.Save()
}

// DeleteUser attempts to delete the target user, returns true is successful
func DeleteUser(id string) bool {
	err := DC.Delete(getUserPath(id))
	return err == nil
}

func getUserPath(id string) string {
	return "data/users/" + id + ".json"
}
