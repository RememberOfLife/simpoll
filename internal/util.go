package internal

import (
	"regexp"
	"unicode"
)

var (
	// IsFileSafe provides a regex to match strings against to check their sanity
	IsFileSafe = regexp.MustCompile(`^[A-Za-z0-9+-]+$`).MatchString
)

func isErr(err error) bool {
	if err == nil {
		return false
	}
	return true
}

func notErr(err error) bool {
	return !isErr(err)
}

func isASCII(s string) bool {
	for i := 0; i < len(s); i++ {
		if s[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}
