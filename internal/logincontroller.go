package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type formLoginCredentials struct {
	UserID   string `form:"userID"`
	Password string `form:"password"`
}

func (form formLoginCredentials) isSafe() bool {
	return IsFileSafe(form.UserID)
}

// GetLoginPage serves the login page
func GetLoginPage(c *gin.Context) {
	SHTML(c, http.StatusOK, "pages/login.html", gin.H{
		"valid": true,
	})
}

// PostLoginCredentials tries to login with the posted credentials
// result page varies depending on result
func PostLoginCredentials(c *gin.Context) {
	var creds formLoginCredentials
	err := c.ShouldBind(&creds)
	if isErr(err) {
		ServeErrorPage(c, http.StatusBadRequest, "Authentication failed, bad post form inputs.")
		return
	}
	if !creds.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe form, ignoring.")
		return
	}
	sessionSetUserIDByCreds(c, creds)
	if sessionGetUserID(c) != "" {
		SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+"/list")
		return
	}
	SHTML(c, http.StatusOK, "pages/login.html", gin.H{
		"valid": false,
	})
}

// GetPerformLogout unsets the current user from the store
// must be the only cookie action performed to work properly
func GetPerformLogout(c *gin.Context) {
	sessionSetUserID(c, "")
	SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+"/login")
}
