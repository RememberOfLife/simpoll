package internal

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// GetVotePage serves the page where one can vote on a poll
func GetVotePage(c *gin.Context) {
	var id uriPollID
	if err := c.ShouldBindUri(&id); err != nil {
		ServeErrorPage(c, http.StatusBadRequest, "Could not bind poll id from uri.")
		return
	}
	if !id.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe pollID, ignored.")
		return
	}
	poll, ok := LoadPoll(id.ID)
	if !ok {
		ServeErrorPage(c, http.StatusBadRequest, "Could not find target poll.")
		return
	}
	dupe := false
	switch poll.AntiDupe {
	case "ip":
		if checkIsDupeIP(id.ID, MB64E.EncodeToString([]byte(c.ClientIP())), false) {
			dupe = true
		}
	case "cookie":
		if checkIsDupeCookie(id.ID, c, false) {
			dupe = true
		}
	default:
	}
	SHTML(c, http.StatusOK, "pages/vote.html", gin.H{
		"id":   id.ID,
		"poll": poll,
		"dupe": dupe,
	})
}

type formRadioVote struct {
	TargetIndex int `form:"radioOption"`
}

// PostVoteRegister registers valid votes on a poll
func PostVoteRegister(c *gin.Context) {
	var id uriPollID
	if err := c.ShouldBindUri(&id); err != nil {
		ServeErrorPage(c, http.StatusBadRequest, "Could not bind poll id from uri.")
		return
	}
	if !id.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe pollID, ignored.")
		return
	}
	poll, ok := LoadPoll(id.ID)
	if !ok {
		ServeErrorPage(c, http.StatusBadRequest, "Could not find target poll.")
		return
	}
	if !poll.Active {
		ServeErrorPage(c, http.StatusBadRequest, "This poll is not active for voting.")
		return
	}
	// check if this is a dupe vote and go error if it is
	switch poll.AntiDupe {
	case "ip":
		if checkIsDupeIP(id.ID, MB64E.EncodeToString([]byte(c.ClientIP())), true) {
			ServeErrorPage(c, http.StatusBadRequest, "Duplicate votes not permitted.")
			return
		}
	case "cookie":
		if checkIsDupeCookie(id.ID, c, true) {
			ServeErrorPage(c, http.StatusBadRequest, "Duplicate votes not permitted.")
			return
		}
	default:
	}
	switch poll.Type {
	case "multi":
		votes := c.PostFormArray("multiOptions")
		// check that num of options picked does not exceed maximum
		if len(votes) > poll.MultiNum || len(votes) < 1 {
			// as this is enforced via javascript client side, if it breaks the user is at fault such that no soft error is required
			ServeErrorPage(c, http.StatusBadRequest, "Either too many or not enough votes given for this poll.")
			return
		}
		// check against duplicates
		voteMap := make(map[int]bool)
		for _, vote := range votes {
			i, err := strconv.Atoi(vote)
			if isErr(err) {
				ServeErrorPage(c, http.StatusBadRequest, "Multi vote option not an index.")
				return
			}
			if _, value := voteMap[i]; !value {
				voteMap[i] = true
			} else {
				ServeErrorPage(c, http.StatusBadRequest, "Multiple votes on the same option disallowed.")
				return
			}
		}
		// accumulate votes on the poll options
		for k := range voteMap {
			poll.Options[k].Votes++
		}
		// accumulate total
		poll.TotalVotes += len(votes)
	case "radio":
		var vote formRadioVote
		err := c.ShouldBind(&vote)
		if isErr(err) {
			ServeErrorPage(c, http.StatusBadRequest, "Could not bind the selected poll option.")
			return
		}
		if vote.TargetIndex < 0 || vote.TargetIndex >= len(poll.Options) {
			ServeErrorPage(c, http.StatusBadRequest, "Could not find the target poll option to vote on.")
			return
		}
		poll.Options[vote.TargetIndex].Votes++
		poll.TotalVotes++
	default:
		ServeErrorPage(c, http.StatusInternalServerError, "Undefined poll type.")
		return
	}
	if !SavePoll(id.ID, poll) {
		ServeErrorPage(c, http.StatusInternalServerError, "Could not save poll.")
		return
	}
	SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+"/r/"+id.ID)
}
