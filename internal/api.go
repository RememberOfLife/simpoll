package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type statusTest struct {
	Type   string `json:"type"`
	Result bool   `json:"result"`
}

// APIGetStatus serves a status message for other services which test this services health
func APIGetStatus(c *gin.Context) {
	token := c.Param("token")[1:]
	elevated := GCONF.StatusAuthzToken != "" && token == GCONF.StatusAuthzToken
	performedTests := []statusTest{statusTest{"statusResponse", true}}
	if elevated {
		// perform in-depth tests
		// currently none
	}
	c.JSON(http.StatusOK, gin.H{
		"msg":            "healthy",
		"elevated":       elevated,
		"performedTests": performedTests,
	})
}

// APIGetPollInfo serves the raw json of the target poll given in the uri id
func APIGetPollInfo(c *gin.Context) {
	var uri uriPollID
	if err := c.ShouldBindUri(&uri); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": "failed to bind pollID"})
		return
	}
	if !uri.isSafe() {
		c.JSON(http.StatusBadRequest, gin.H{"msg": "unsafe pollID"})
		return
	}
	poll, ok := LoadPoll(uri.ID)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"msg": "poll not found"})
		return
	}
	// set all votes to -1 is results are closed
	poll.TotalVotes = -1
	for i := range poll.Options {
		poll.Options[i].Votes = -1
	}
	c.JSON(http.StatusOK, gin.H{"poll": poll})
}

type formAction struct {
	PollID     string `form:"pollID"`
	Action     string `form:"action"`
	ReturnPath string `form:"returnPath"`
}

func (form formAction) isSafe() bool {
	return IsMB64E(form.PollID)
}

// APIPostPerformPollAction performs the supplied poll action on the selected poll
func APIPostPerformPollAction(c *gin.Context) {
	var action formAction
	err := c.ShouldBind(&action)
	if isErr(err) {
		ServeErrorPage(c, http.StatusBadRequest, "Could not bind the target action to perform.")
		return
	}
	if !action.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe form action for poll, ignored.")
		return
	}
	switch action.Action {
	case "toggleActive":
		var poll *Poll
		poll, ok := LoadPoll(action.PollID)
		if !ok {
			ServeErrorPage(c, http.StatusInternalServerError, "Failed to load poll.")
			return
		}
		poll.Active = !poll.Active
		if !SavePoll(action.PollID, poll) {
			ServeErrorPage(c, http.StatusInternalServerError, "Could not save poll.")
			return
		}
	case "releaseResults":
		var poll *Poll
		poll, ok := LoadPoll(action.PollID)
		if !ok {
			ServeErrorPage(c, http.StatusInternalServerError, "Failed to load poll.")
			return
		}
		poll.ResultsPublic = true
		if !SavePoll(action.PollID, poll) {
			ServeErrorPage(c, http.StatusInternalServerError, "Could not save poll.")
			return
		}
	case "delete":
		if !DeletePoll(action.PollID) {
			ServeErrorPage(c, http.StatusInternalServerError, "Could not delete poll.")
			return
		}
		SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+"/list")
		return
	default:
		ServeErrorPage(c, http.StatusInternalServerError, "Undefined action.")
		return
	}
	SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+action.ReturnPath)
}
