package internal

import (
	"encoding/base64"
	"regexp"
)

var (
	// MB64E provides a modified encoding of base64 that should work even in urls and filenames
	MB64E = base64.NewEncoding("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-")
	// IsMB64E provides a regex to match ids against to check their sanity
	IsMB64E = regexp.MustCompile(`^[A-Za-z0-9+-]{8}$`).MatchString
)
