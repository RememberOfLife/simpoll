package internal

import "github.com/gin-gonic/gin"

func sessionGetUserID(c *gin.Context) string {
	return c.MustGet("rcs_userID").(string)
}

func sessionSetUserID(c *gin.Context, userID string) {
	c.Set("rcs_userID", userID)
}

func sessionSetUserIDByCreds(c *gin.Context, creds formLoginCredentials) {
	user, ok := LoadUser(creds.UserID)
	if !ok {
		sessionSetUserID(c, "")
		return
	}
	hash, ok := getUserPasswordHash(user.Salt, creds.Password)
	if user.PasswordHash != hash || !ok {
		sessionSetUserID(c, "")
		return
	}
	sessionSetUserID(c, user.ID)
}

func sessionGetVotedOnIDs(c *gin.Context) []string {
	votedOnIDs := c.MustGet("rcs_votedOnIDs")
	if votedOnIDs == nil {
		return nil
	}
	return votedOnIDs.([]string)
}

func sessionSetVotedOnIDs(c *gin.Context, votedOnIDs []string) {
	c.Set("rcs_votedOnIDs", votedOnIDs)
}
