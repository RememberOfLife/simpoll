package internal

import (
	"net/http"
	"sort"

	"github.com/gin-gonic/gin"
)

// GetResultsPage serves the results page for a poll
func GetResultsPage(c *gin.Context) {
	var id uriPollID
	if err := c.ShouldBindUri(&id); err != nil {
		ServeErrorPage(c, http.StatusBadRequest, "Could not bind poll id from uri.")
		return
	}
	if !id.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe pollID, ignored.")
		return
	}
	poll, err := LoadPoll(id.ID)
	if !err {
		ServeErrorPage(c, http.StatusBadRequest, "Could not find target poll.")
		return
	}
	if poll.ResultsPublic {
		// sort poll option by most votes descending, only if results public though!
		sort.Slice(poll.Options, func(i, j int) bool {
			return poll.Options[i].Votes > poll.Options[j].Votes
		})
	}
	SHTML(c, http.StatusOK, "pages/results.html", gin.H{
		"id":   id.ID,
		"poll": poll,
	})
}
