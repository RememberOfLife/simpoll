package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ServeErrorPage serves an error with the given status and message
// it does abort the context but the caller has to manually stop their own control-flow e.g. by returning
func ServeErrorPage(c *gin.Context, sc int, errorDesc string) {
	SHTML(c, http.StatusOK, "pages/error.html", gin.H{
		"statusCode": sc,
		"statusMsg":  http.StatusText(sc),
		"errorDesc":  errorDesc,
	})
	c.Abort() // this does not stop execution
}
