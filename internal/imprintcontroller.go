package internal

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

// GetImprintPage serves the imprint
func GetImprintPage(c *gin.Context) {
	_, err := os.Stat("templates/custom/imprint.html")
	SHTML(c, http.StatusOK, "pages/imprint.html", gin.H{
		"default": err != nil,
	})
}
