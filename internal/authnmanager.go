package internal

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

/*
Session Rewrite:

| fix isAuthn everywhere so it isnt used anymore

| SessionMiddleware:
	calls GenerateSessionFromContext

| GenerateSessionFromContext:
	sets the request-context variables to the proper values given by the cookie

| SessionSaveAll:
	saves all attributes from their request-context variables to the cookie

| Session[Set/Get][attrName]:
	set or load the raw value of the attrName in the request-context variables

| SessionUserIDSetByCreds:
	set the userID in the request-context variable by the given credentials
*/

/*
SESSION ATTRIBUTES:
userID
votedOnIDs
*/

func sessionPrintALL(c *gin.Context) {
	fmt.Println("SESSION SAVING:")
	if userID := sessionGetUserID(c); userID == "" {
		fmt.Printf("[nil]\n")
	} else {
		fmt.Printf("[%s]\n", userID)
	}
	if votedOnIDs := sessionGetVotedOnIDs(c); votedOnIDs == nil {
		fmt.Println("NIL")
	} else {
		for _, v := range votedOnIDs {
			fmt.Println(v)
		}
	}
}

// GenerateSessionFromContext extracts the current session from the context cookie if applicable
// saves all session attributes into their respective request-context variables
func generateSessionFromContext(c *gin.Context) {
	//cookieSession := sessions.Default(c)
	cookieSession := sessions.Default(c)
	c.Set("rcs_userID", "")
	rawUserID := cookieSession.Get("userID")
	if rawUserID != nil {
		userID := rawUserID.(string)
		if _, ok := LoadUser(userID); ok {
			c.Set("rcs_userID", rawUserID)
		}
	}
	c.Set("rcs_votedOnIDs", nil)
	rawVotedOnIDs := cookieSession.Get("votedOnIDs")
	if rawVotedOnIDs != nil {
		c.Set("rcs_votedOnIDs", strings.Split(rawVotedOnIDs.(string), ";"))
	}
}

// ContextSessions makes a session reference available for every context
func ContextSessions() gin.HandlerFunc {
	return func(c *gin.Context) {
		generateSessionFromContext(c)

		c.Next()
	}
}

// SaveTS saves the session to the context store
func sessionSaveAll(c *gin.Context) {
	//cookieSession := sessions.Default(c)
	cookieSession := sessions.Default(c)
	if userID := sessionGetUserID(c); userID != "" {
		cookieSession.Set("userID", userID)
	} else {
		cookieSession.Set("userID", nil)
	}
	if votedOnIDs := sessionGetVotedOnIDs(c); len(votedOnIDs) > 0 {
		cookieSession.Set("votedOnIDs", strings.Join(votedOnIDs, ";"))
	} else {
		cookieSession.Set("votedOnIDs", nil)
	}
	// set cookie options
	cookieSession.Options(sessions.Options{
		Path:     "/",
		MaxAge:   60 * 60 * 12,
		Secure:   true,
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
	})
	cookieSession.Save()
	sessionPrintALL(c)
}
