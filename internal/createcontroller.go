package internal

import (
	"crypto/rand"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetCreatePage serves the creation page for a new poll
func GetCreatePage(c *gin.Context) {
	SHTML(c, http.StatusOK, "pages/create.html", gin.H{})
}

type formPollCreation struct {
	Title              string `form:"title"`
	Description        string `form:"description"`
	Type               string `form:"type"`
	MultiNum           int    `form:"multiNum"`
	AntiDupe           string `form:"antiDupe"`
	StartActive        bool   `form:"startActive"`
	StartResultsPublic bool   `form:"startResultsPublic"`
}

func (form formPollCreation) isSafe() bool {
	return isASCII(form.AntiDupe)
}

// PostCreatePoll creates a new poll with the post form data, then redirects there
func PostCreatePoll(c *gin.Context) {
	var form formPollCreation
	err := c.ShouldBind(&form)
	if isErr(err) {
		ServeErrorPage(c, http.StatusBadRequest, "Could not process request, bad post form.")
		return
	}
	if !form.isSafe() {
		ServeErrorPage(c, http.StatusBadRequest, "Unsafe form, ignoring.")
		return
	}
	// bind options extra because gin does not support binding arrays in form structs as of now
	options, ok := c.GetPostFormArray("options")
	if !ok {
		ServeErrorPage(c, http.StatusBadRequest, "Could not process request, bad post form.")
		return
	}
	newPoll := new(Poll)
	newPoll.Title = form.Title
	newPoll.Description = form.Description
	newPoll.Type = form.Type
	switch form.Type {
	case "radio":
		newPoll.MultiNum = 1
	case "multi":
		if form.MultiNum < 1 {
			form.MultiNum = len(options)
		}
		newPoll.MultiNum = form.MultiNum
	default:
		ServeErrorPage(c, http.StatusBadRequest, "Could not process post form, unknown poll tpye.")
		return
	}
	newPoll.AntiDupe = form.AntiDupe
	newPoll.Active = form.StartActive
	newPoll.ResultsPublic = form.StartResultsPublic
	newPoll.TotalVotes = 0
	newPoll.Options = make([]PollOption, len(options))
	for i, opt := range options {
		newPoll.Options[i] = PollOption{opt, 0}
	}
	// poll id determination
	idBytes := make([]byte, 6) // 4chars = 3bytes --> ids are length 8
	rand.Read(idBytes)
	// custom encoder
	pollID := MB64E.EncodeToString(idBytes)
	for PollExists(pollID) {
		rand.Read(idBytes)
		pollID = MB64E.EncodeToString(idBytes)
	}
	newPoll.ID = pollID
	if !createAntiDupe(pollID) {
		ServeErrorPage(c, http.StatusInternalServerError, "Could not create anti dupe.")
		return
	}
	if !SavePoll(pollID, newPoll) {
		ServeErrorPage(c, http.StatusInternalServerError, "Could not save poll.")
		return
	}
	SRedirect(c, http.StatusSeeOther, GCONF.WebRoot+"/v/"+pollID)
}
