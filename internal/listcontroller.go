package internal

import (
	"io/ioutil"
	"net/http"
	"sort"
	"strings"

	"github.com/gin-gonic/gin"
)

// GetListPage serves a page containing an entry for every active poll
func GetListPage(c *gin.Context) {
	pollFiles, err := ioutil.ReadDir("data/polls/")
	if isErr(err) {
		ServeErrorPage(c, http.StatusInternalServerError, "Could not load polls.")
		return
	}
	var polls []*Poll
	for _, f := range pollFiles {
		p, b := LoadPoll(strings.TrimSuffix(f.Name(), ".json"))
		if b {
			polls = append(polls, p)
		}
	}
	// sort polls by title
	sort.Slice(polls, func(i, j int) bool {
		return polls[i].Title < polls[j].Title
	})
	// get all polls
	SHTML(c, http.StatusOK, "pages/list.html", gin.H{
		"count": len(polls),
		"polls": polls,
	})
}
