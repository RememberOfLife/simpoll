package internal

import (
	"github.com/gin-gonic/gin"
)

// SHTML wraps c.HTML to automatically append the current session to the handler map
func SHTML(c *gin.Context, code int, location string, valmap gin.H) {
	valmap["userID"] = sessionGetUserID(c)
	sessionSaveAll(c)
	valmap["navTarget"] = location
	c.HTML(code, location, valmap)
}

// SRedirect wraps c.Redirect to automatically save the session
func SRedirect(c *gin.Context, code int, location string) {
	sessionSaveAll(c)
	c.Redirect(code, location)
}
