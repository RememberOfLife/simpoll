package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GenericErrors serves an error page showing 404 for all unmapped routings that are requested
func GenericErrors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next() // lets everything pass if anything, only then tests if in the end this would be a failure
		if c.Writer.Status() == http.StatusNotFound {
			ServeErrorPage(c, http.StatusNotFound, "There is no mapped route to the requested path.") // aborts so we dont have to
		}
	}
}
