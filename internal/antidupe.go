package internal

import (
	"crypto/rand"

	"github.com/gin-gonic/gin"
)

// AntiDupe represents a list of poll access entries as saved in the json db
type AntiDupe struct {
	ID      string              `json:"id"`
	Salt    string              `json:"salt"`
	Entries map[string]struct{} `json:"entries"`
}

func createAntiDupe(id string) bool {
	ad := new(AntiDupe)
	ad.ID = id
	ad.Entries = make(map[string]struct{})
	// create new salt
	salt := make([]byte, 64)
	rand.Read(salt)
	ad.Salt = MB64E.EncodeToString(salt)
	return notErr(DC.Save(getAntiDupePath(id), &ad))
}

func deleteAntiDupe(id string) bool {
	return notErr(DC.Delete(getAntiDupePath(id)))
}

// checkIsDupeIP returns true if the provided data is a duplicate or the operation failed
// this function automatically writes the data into the map if it doesnt exist yet
func checkIsDupeIP(id string, data string, saveAccess bool) bool {
	var ad AntiDupe
	if err := DC.Load(getAntiDupePath(id), &ad); isErr(err) {
		return true
	}
	//TODO: using userPW hash for now, should be moved to common crypto class or sessions management
	dhash, ok := getUserPasswordHash(ad.Salt, data)
	if !ok {
		return true
	}
	_, ok = ad.Entries[dhash]
	if saveAccess && !ok {
		ad.Entries[dhash] = struct{}{}
		if err := DC.Save(getAntiDupePath(id), &ad); isErr(err) {
			return true
		}
	}
	return ok
}

func getAntiDupePath(id string) string {
	return "data/antidupe/" + id + ".json"
}

// checkIsDupeCookie returns true if the provided context has already voted on this poll or the operation failed
// this function automatically saves the id into the cookie if it doesnt exist yet
func checkIsDupeCookie(id string, c *gin.Context, saveAccess bool) bool {
	votedOnIDs := sessionGetVotedOnIDs(c)
	dupe := false
	// check if exists
	for _, v := range votedOnIDs {
		if v == id {
			dupe = true
			break
		}
	}
	// if not ex. and write -> add to cookies list
	if !dupe && saveAccess {
		if votedOnIDs == nil {
			votedOnIDs = []string{id}
		} else {
			votedOnIDs = append(votedOnIDs, id)
		}
		sessionSetVotedOnIDs(c, votedOnIDs)
	}
	return dupe
}
