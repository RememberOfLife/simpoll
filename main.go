package main

import (
	"bufio"
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"

	"gitlab.com/RememberOfLife/simpoll/internal"
)

func main() {
	// escape if only user management function is required
	argsWithoutProg := os.Args[1:]
	if performUserAction(argsWithoutProg) {
		internal.DC.Close()
		internal.DC.WriteBackAll()
		return
	}

	// set release mode if not debug
	if !internal.GCONF.DebugMode {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.New()

	// use middlewares
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(internal.GenericErrors()) // serves errors for unregistered routings
	// enable sessions for use in authn, use a random secret on every startup
	ak, ek, ako, eko := internal.SessionGetKeys()
	var sessionsStore cookie.Store
	if ako != nil && eko != nil {
		sessionsStore = cookie.NewStore(ak, ek, ako, eko)
	} else {
		sessionsStore = cookie.NewStore(ak, ek)
	}
	router.Use(sessions.Sessions("session_simpoll", sessionsStore))
	router.Use(internal.ContextSessions()) // makes a session reference available for every context

	// register statics
	router.Static("res/", "res") // resource path
	router.SetFuncMap(template.FuncMap{
		"WR": internal.GetWebRoot, // defines the WebRoot for all templates
	})
	router.LoadHTMLGlob("templates/**/*") // everything recursively in the template folder

	// register routings
	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusSeeOther, internal.GCONF.WebRoot+"/list")
	})

	router.GET("/imprint", internal.GetImprintPage)

	router.GET("/login", internal.GetLoginPage)
	router.POST("/login", internal.PostLoginCredentials)
	router.GET("/logout", internal.GetPerformLogout)

	router.GET("/list", internal.RequireSessionAuthn(), internal.GetListPage)

	router.GET("/create", internal.RequireSessionAuthn(), internal.GetCreatePage)
	router.POST("/create", internal.RequireSessionAuthn(), internal.PostCreatePoll)

	router.GET("/v/:pollID", internal.GetVotePage)
	router.POST("/v/:pollID", internal.PostVoteRegister)

	router.GET("/r/:pollID", internal.GetResultsPage)

	api := router.Group("/api")
	{
		api.GET("/status/*token", internal.APIGetStatus)
		api.GET("/poll/:pollID", internal.APIGetPollInfo)
		api.POST("/poll", internal.RequireSessionAuthn(), internal.APIPostPerformPollAction)
	}

	if gin.Mode() == gin.DebugMode {
		dbg := router.Group("/debug")
		{
			dbg.GET("/dbam", internal.DBGGetDBAccessMap)
		}
	}

	// use http.Server for graceful shutdown, replaces
	//router.Run(":" + internal.GCONF.LocalPort) // listen and serve on local port given in config
	srv := &http.Server{
		Addr:    ":" + internal.GCONF.LocalPort,
		Handler: router,
	}

	// goroutine the server, taken from gin-doc
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// wait for interrupt signal to gracefully shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("shuting down server...")

	// inform the server it has 5 seconds to finish
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("server forced to shutdown:", err)
	}

	// write-back unsaved jdba cache
	internal.DC.Close()
	internal.DC.WriteBackAll()

	log.Println("Server exiting")
}

func performUserAction(args []string) bool {
	if len(args) == 0 {
		return false
	}
	// switch on the required action
	switch args[0] {
	case "adduser":
		// read username and password then create user
		if len(args) == 3 {
			if !internal.CreateUser(args[1], args[2]) {
				fmt.Println("auto user creation failed..")
			}
		} else {
			reader := bufio.NewReader(os.Stdin)
			fmt.Println("creating new user, enter new userID:")
			un, err := reader.ReadString('\n')
			fmt.Println("enter password for the new user:")
			pw, err := reader.ReadString('\n')
			un, pw = strings.Trim(un, " \n"), strings.Trim(pw, " \n")
			if !internal.CreateUser(un, pw) || err != nil {
				fmt.Println("interactive user creation failed..")
			}
		}
		return true
	case "deluser":
		if len(args) < 2 {
			return true
		}
		fmt.Println("removing user: " + args[1])
		if !internal.DeleteUser(args[1]) {
			fmt.Println("user deletion failed..")
		}
		return true
	default:
		fmt.Println("unknown operation, exiting")
		return true
	}
}
