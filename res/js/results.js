(function(){
    $(".cdh-percentage").each(function() {
        var target = $(this);
        var pp = target.data("percentage").split("/");
        var tp = ((pp[0] / pp[1] * 100) || 0).toFixed(2);
        target.text(tp.concat(target.text()));
    });
})();
