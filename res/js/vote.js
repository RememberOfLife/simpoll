function validateVoteForm(cur) {
    var form = $(cur);
    var feedback = $("#formValidationError");
    // get all inputs in the form and determine how many are checked
    var inputs = form.find("input");
    var checkedCount = 0;
    inputs.each(function( index ) {
        var fecur = $(this);
        if (fecur.is(":checked")) {
            checkedCount++;
        }
    });
    // switch by type and show error if checkedCount is outside allowed range
    var type = form.data("type");
    switch (type) {
        case "radio":
            if (checkedCount != 1) {
                feedback.text("You need to select exactly 1 option for your vote to be registered.");
                return false;
            }
            break;
        case "multi":
            if (checkedCount < 1) {
                feedback.text("You need to select at least 1 option for your vote to be registered.");
                return false;
            }
            var multinum = form.data("multinum")
            plural = (multinum > 1) ? "s":"";
            if (checkedCount > multinum) {
                feedback.text("You need to select at maximum " + multinum + " option" + plural + " for your vote to be registered.");
                return false;
            }
            break;
        default:
            feedback.text("undefined poll type");
            return false;
    }
    return true;
}
