$("#btnAddOption").on("click", function() {
    // get opt list and new opt id
    var optList = $("#optionList");
    var noi = (optList.children().length-1);
    // get new opt from template
    var newOpt = $("#optionBASE").clone();
    newOpt.removeAttr("hidden");
    // properly set the input
    var newOptInput = newOpt.find("#optionIBASE");
    newOptInput.removeAttr("disabled");
    newOptInput.prop("id", "optionI" + noi);
    newOptInput.prop("name", "options")
    // properly set the deletion button
    var newOptDelBtn = newOpt.find("#optionDelBASE");
    newOptDelBtn.prop("id", "optionDel" + noi);
    newOptDelBtn.data("delid", noi);
    // append to optionList
    newOpt.prop("id", "option" + noi);
    optList.append(newOpt);
});

function removeOption(cur) {
    // proper deletion without id chain breaking
    var index = $(cur).data("delid");
    var target = $("#option" + index);
    var renameTargets = target.nextAll();
    target.remove();
    // rename everything hereon
    renameTargets.each(function( newIndex ) {
        // respec top-level id, input id and name, del btn id and data
        var fecur = $(this);
        var rIndex = index + newIndex;
        // properly set the input
        var eOptInput = fecur.find("#optionI" + (rIndex + 1));
        eOptInput.removeAttr("disabled");
        eOptInput.prop("id", "optionI" + rIndex);
        // properly set the deletion button
        var eOptDelBtn = fecur.find("#optionDel" + (rIndex + 1));
        eOptDelBtn.prop("id", "optionDel" + rIndex);
        eOptDelBtn.data("delid", rIndex);
        // set top-level
        fecur.prop("id", "option" + rIndex);
    });
}
