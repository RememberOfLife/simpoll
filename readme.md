# simpoll

Simple poll tool for quick votings, written in go.  
Intended to be used behind a reverse proxy like nginx.

## Features

* Bootstrap design with big fonts and buttons
* Poll activation on command
* Hiding poll results until published
* Restricting multiple choice polls to a certain number of answers

## How To Use

* `$ go get gitlab.com/RememberOfLife/simpoll` to clone it to your local go path
* navigate inside the simpoll directory: (`$GOPATH/gitlab.com/RememberOfLife/simpoll`)
* `$ cp default-config.json config.json` and edit your custom config as seen below
* `$ go build -o simpoll main.go` to build the binary for your platform
* `$ simpoll adduser <userID> <password>` to create a new user for yourself (see below)
* setup a systemd userservice for simpoll and a location block in your nginx (don't forget to set both `X-Real-IP` and `X-Forwarded-For` or the ip de-duplication won't work)

You may want to set the origin to ssh with:  
`$ git remote set-url origin git@gitlab.com:RememberOfLife/simpoll.git`

## Configuration

Custom configuration options for the entire service are saved in `config.json` it is recommended not to edit the fallback provided in `default-config.json`.  
If you wish to use this service on an outward facing domain such as `example.com/` and possibly even a set sub path such as `example.com/some/path/to/simpoll/` you have to set your port to something configured in your nginx location block as the forward, and especially the webRoot to the full url to that location excluding any trailing slashes. Example: `https://example.com` and respectively `https://example.com/some/path/to/simpoll`; the location should match the location block in your nginx config.

You may want to customize your instances imprint to comply with any laws that apply to you. Creating the file `templates/custom/imprint.html` will enable that custom imprint, it may contain html like any other fragment.  
*This is a fully trusted file, it is highly discouraged to use any custom JS or CSS in this file. Doing so without proper knowledge may create an unchecked security risk for you and your users.*

## User Management

The main binary also provides a functionality for adding users.  
*Please note all usernames must adhere to the filesafe regex: `^[A-Za-z0-9+-]+$`*  
`$ simpoll adduser` starts the interactive process. You will be asked for a userID and password for the user.  
`$ simpoll adduser <userID> <password>` if you're automating the process.  
`$ simpoll deluser <userID>` to delete, or manually remove their account from the `data/users/` directory.
